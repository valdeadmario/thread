import React, { Component } from "react";
import { Label, Icon, Modal, Button, Form } from "semantic-ui-react";

import styles from "../Post/styles.module.scss";

export default class UpdatedModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            updatedBody: this.props.body,
            modalOpen: false
        };
    }

    onUpdateSubmit = () => {
        if (this.state.updatedBody) {
            if (this.state.updatedBody !== this.props.body)
                this.props.updatePost(this.props.id, this.state.updatedBody);
            this.setState({ updatePost: "" });
            this.handleClose();
        }
    };
    handleOpen = () => this.setState({ modalOpen: true });

    handleClose = () => this.setState({ modalOpen: false });

    render() {
        return (
            <Modal
                open={this.state.modalOpen}
                onClose={this.handleClose}
                basic
                size="small"
                centered={true}
                trigger={
                    <Label
                        onClick={this.handleOpen}
                        basic
                        size="small"
                        as="a"
                        className={styles.toolbarBtn}
                    >
                        <Icon name="edit" />
                    </Label>
                }
            >
                <Modal.Content image>
                    <Form onSubmit={this.onUpdateSubmit}>
                        <Form.TextArea
                            width="80%"
                            name="body"
                            value={this.state.updatedBody}
                            placeholder="What is the news?"
                            onChange={ev => {
                                console.log(this.state);
                                this.setState({ updatedBody: ev.target.value });
                            }}
                        />
                        <Button floated="right" color="blue" type="submit">
                            Update post
                        </Button>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}
