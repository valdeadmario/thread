import * as postService from "src/services/postService";
import * as commentService from "src/services/commentService";
import {
    ADD_POST,
    UPDATE_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST
} from "./actionTypes";

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const updatePostAction = post => ({
    type: UPDATE_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

export const loadPosts = filter => async dispatch => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const {
        posts: { posts }
    } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts.filter(
        post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const updatePost = (id, data) => async (dispatch, getRootState) => {
    await postService.updatePost(id, data);
    //const posts = postService.getAllPosts();
    const {
        posts: { posts }
    } = getRootState();
    const updated = posts.map(post => {
        let item;
        if (post.id === id) {
            item = {
                ...post,
                data
            };
        } else {
            item = post;
        }
        return item;
    });
    dispatch(setPostsAction(updated));
};

export const deletePost = postId => async (dispatch, getRootState) => {
    await postService.deletePost(postId);
    const {
        posts: { posts }
    } = getRootState();
    const idx = posts.findIndex(post => post.id === postId);
    const newPosts = [...posts.slice(0, idx), ...posts.slice(idx)];
    dispatch(setPostsAction(newPosts));
};

export const toggleExpandedPost = postId => async dispatch => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

const counterDiff = (like, isLike, createdAt, updatedAt) => {
    let likeDiff = 0;
    let dislikeDiff = 0;
    switch (isLike) {
        case true:
            likeDiff = 1;
            if (createdAt !== updatedAt) {
                dislikeDiff = -1;
            }
            break;
        case false:
            dislikeDiff = 1;
            if (createdAt !== updatedAt) {
                likeDiff = -1;
            }
            break;
        default:
            if (like) {
                likeDiff = -1;
            } else {
                dislikeDiff = -1;
            }
    }
    return {
        likeDiff,
        dislikeDiff
    };
};

export const likePost = postId => async (dispatch, getRootState) => {
    const { isLike, createdAt, updatedAt } = await postService.likePost(postId);

    const { likeDiff, dislikeDiff } = counterDiff(
        true,
        isLike,
        createdAt,
        updatedAt
    );

    const mapLikes = post => ({
        ...post,
        likeCount: Number(post.likeCount) + likeDiff,
        dislikeCount: Number(post.dislikeCount) + dislikeDiff
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post =>
        post.id !== postId ? post : mapLikes(post)
    );

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
    const { isLike, createdAt, updatedAt } = await postService.dislikePost(
        postId
    );
    const { likeDiff, dislikeDiff } = counterDiff(
        false,
        isLike,
        createdAt,
        updatedAt
    );

    const mapDislikes = post => ({
        ...post,
        likeCount: Number(post.likeCount) + likeDiff,
        dislikeCount: Number(post.dislikeCount) + dislikeDiff
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post =>
        post.id !== postId ? post : mapDislikes(post)
    );

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
    }
};

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post =>
        post.id !== comment.postId ? post : mapComments(post)
    );

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};
